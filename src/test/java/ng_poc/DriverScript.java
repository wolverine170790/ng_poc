package ng_poc;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.ExtentReports;
import utilities.Custom_Actions;
import utilities.ExcelPack;

public class DriverScript extends Setup {
	
	@BeforeClass
	public void buildUp() {
		reporter = new ExtentReports(System.getProperty("user.dir") + "\\test-output\\ExtentReportsVault\\ExtentReportResults.html");
	}
	
	@BeforeMethod
	public void appSetup() throws InvocationTargetException, InterruptedException {
		test_report = reporter.startTest(ExcelPack.gettestcasename());
		Setup.load_propfile();
		
		
	}
	@Test(priority = 1,enabled = true)
	public void mx_create_pm() throws InterruptedException {
		String browser = ExcelPack.getCelldata(1,0);
		Setup.launchBrowser(browser);
		Maximo.mx_injecturl();
		Maximo.mx_login();
		//Custom_Actions.waitforelementonscreen(20,"mx_goto_button","just_wait");
		//Maximo.mx_goto("Preventive Maintenance","Preventive Maintenance");
		//This is a change to trigger
		Maximo.mx_logout();
		
	}
	
	@Test(priority = 2,enabled = false,groups="salesforce")
	public void sf_search_workorder() throws InterruptedException, IOException, ParseException {
		String browser = ExcelPack.getCelldata(1,5);
		Setup.launchBrowser(browser);
		Salesforce.sf_injecturl();
		Salesforce.sf_login();
		//Salesforce.sf_drag_and_drop();
		Salesforce.sf_search_workorder();
		Salesforce.sf_logout();
	}
	
	
	@AfterMethod
	public void tearDown() {
		Setup.closeBrowser();
		
	}
	
	@AfterClass
	public void flushDown() {
		reporter.endTest(test_report);
		reporter.flush();
	}
	




}

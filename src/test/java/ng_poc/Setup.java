package ng_poc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Setup {
	protected static Properties prop = new Properties();
	public static WebDriver driver;
	protected static ChromeOptions options = new ChromeOptions();
	protected static ExtentReports reporter;
	protected static ExtentTest test_report;
	protected static WebDriverWait waitobj;
	
	public static void load_propfile() {
		  
		  InputStream input = null;
		  
		  try {
			input = new FileInputStream("object.properties");
			prop.load(input);
			test_report.log(LogStatus.PASS, "Properties file loaded successfully");
			
		} catch (FileNotFoundException e) {
			System.out.println("Properties file not found in the directory");
			test_report.log(LogStatus.FAIL, "Properties file not found in the directory");
		} catch (IOException e) {
			System.out.println("Properties file NOT readable");
			test_report.log(LogStatus.FAIL, "Properties file NOT readable");
		}
//
	}

	public static void launchBrowser(String browsername) {

		  switch (browsername) {
		  
		case "chrome":
			try {
				String Chrome_Profile_Path = "C:\\Users\\athangaraj001\\AppData\\Local\\Google\\Chrome\\User Data";
				options.setExperimentalOption("useAutomationExtension", false);
				options.addArguments("start-maximized");
				options.addArguments("chrome.switches","--disable-extensions"); 
				options.addArguments("user-data-dir=" + Chrome_Profile_Path);
				options.addArguments("--disable-dev-shm-usage");
				driver = new ChromeDriver(options);
				test_report.log(LogStatus.PASS, "Chrome browser launched successfully");
				break;
			} catch (Exception e) {
				System.out.println("Error launching the chrome browser");
				test_report.log(LogStatus.FAIL, "Error launching the chrome browser");
			}
			
		case "internet_explorer":
			String IEfilepath = "C:\\Users\\athangaraj001\\Documents\\ng_poc\\src\\test\\resources\\IEDriverServer.exe";
			System.setProperty("webdriver.ie.driver", IEfilepath);
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			break;
		default:
			break;
		}
		  		
	}
	
	
	public static void closeBrowser() {
		
		driver.close();
	}
	


	
}
		

























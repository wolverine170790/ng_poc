package ng_poc;

import java.io.IOException;
import java.text.ParseException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import utilities.Custom_Actions;

public class Salesforce extends Setup {
	
	protected static Actions obj_action = new Actions(driver);
	
	public static void sf_login() throws IOException, InterruptedException {
		

		Custom_Actions.axn_entertextbox_xpath("sf_txt_username", "agron@nationalgrid.com.uat");
		Custom_Actions.axn_entertextbox_xpath("sf_txt_password", "Tester01!");
		Custom_Actions.axn_clickonelement_xpath("sf_btn_login");
		Custom_Actions.axn_waitForElement(60, prop.getProperty("sf_img_logout"));
		Thread.sleep(5000);
	}

	public static void sf_injecturl() throws IOException, ParseException {
		
		driver.get(prop.getProperty("sf_url"));
	}
	
	
	public static void sf_logout() {
		
		Custom_Actions.axn_waitForElement(20,prop.getProperty("sf_img_logout"));
		Custom_Actions.axn_clickonelement_xpath("sf_img_logout");
		Custom_Actions.axn_clickonelement_xpath("sf_link_logout");
		Custom_Actions.waitforelementonscreen(20, "sf_txt_username", "just_wait");

	}
	
	public static void sf_drag_and_drop() {
		
		Custom_Actions.axn_entertextbox_xpath("sf_txt_searchbox","13794");
		WebElement from_element = driver.findElement(By.xpath(prop.getProperty("sf_web_drag_from")));
		WebElement to_element = driver.findElement(By.xpath(prop.getProperty("sf_web_drag_to")));
		obj_action.dragAndDrop(from_element,to_element).build().perform();
		
	}
	
	public static void sf_search_workorder() {
		
		//Custom_Actions.axn_entertextbox_xpath("sf_txt_searchbox","59311");
		WebElement searchbox = driver.findElement(By.xpath(prop.getProperty("sf_txt_searchbox")));
		Actions seriesofactions = obj_action.moveToElement(searchbox).click().sendKeys(searchbox, "59311");
		seriesofactions.build().perform();
		Custom_Actions.axn_clickonelement_xpath("sf_list_searchbox_selection");
		Custom_Actions.waitforelementonscreen(20,"sf_label_WOnumber","just_wait");
	}
}

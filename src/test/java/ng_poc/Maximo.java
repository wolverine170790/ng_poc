package ng_poc;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import ng_poc.Setup;
import utilities.ExcelPack;
import utilities.Reporting_Tools;
import utilities.Validation;
import utilities.Custom_Actions;

public class Maximo extends Setup {
	
	public static Actions act = new Actions(driver);
	
	public static void mx_login() {
		
		  String sUserName = ExcelPack.getCelldata(1,1);
		  String sPassword = ExcelPack.getCelldata(1,2);
		  Custom_Actions.axn_waitForElement(3,prop.getProperty("mx_username"));
		  driver.findElement(By.xpath(prop.getProperty("mx_username"))).sendKeys(sUserName);
		  driver.findElement(By.xpath(prop.getProperty("mx_password"))).sendKeys(sPassword);
		  driver.findElement(By.xpath(prop.getProperty("mx_login"))).click();
	}

	public static void mx_goto(String menuitem, String submenuitem) throws InterruptedException,NullPointerException {
		
		try {
			Custom_Actions.axn_clickonelement_xpath("mx_goto_button");
			Custom_Actions.waitforelementonscreen(30,"//span[text()='" + menuitem + "' and contains(@id,'menu0') and not(contains(@id,'sub'))]","just_wait");
			Reporting_Tools.capture_screenprint("file1");
			WebElement main_menu = driver.findElement(By.xpath("//span[text()='" + menuitem + "' and contains(@id,'menu0') and not(contains(@id,'sub'))]"));
			act.moveToElement(main_menu).click().build().perform();
			Custom_Actions.waitforelementonscreen(30,"//span[starts-with(text(),'" + submenuitem + "') and contains(@id,'_MODULE_sub_') and starts-with(@id,'menu0')]","just_wait");
			WebElement sub_menu = driver.findElement(By.xpath("//span[starts-with(text(),'" + submenuitem + "') and contains(@id,'_MODULE_sub_') and starts-with(@id,'menu0')]"));
			act.moveToElement(sub_menu).click().build().perform();

		} catch (NullPointerException e) {
			System.out.println("Null pointer exception custom");
		} catch (ElementNotVisibleException e) {
			System.out.println("Element is NOT visible or loaded in the application");
		}
		

	}
	
	public static void mx_createnewpm() {
		
		Custom_Actions.axn_clickonelement_xpath(prop.getProperty("mx_new_record_button"));
		
	}
	
	public static void mx_logout() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.xpath(prop.getProperty("mx_logout"))).click();
	}
	
	public static void mx_injecturl() {
		
		driver.get(prop.getProperty("mx_url"));
		Validation.check_title("Welcome to National Grid (USA) TST02");
	}
	
}

package utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import com.google.common.io.Files;
import com.relevantcodes.extentreports.LogStatus;

import ng_poc.Setup;

public class Reporting_Tools extends Setup {
	
	public static void capture_screenprint(String filename) {
		
		String capture_date = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot take_shot = (TakesScreenshot) driver;
		File sourcepath = take_shot.getScreenshotAs(OutputType.FILE);
		String destpath = System.getProperty("user.dir") + "\\test-output\\ExtentReportsVault\\screenshots\\" + filename + "_" + capture_date + ".png";
		File finaldest = new File(destpath);
		try {
			Files.copy(sourcepath,finaldest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		test_report.log(LogStatus.FAIL,test_report.addScreenCapture(destpath));
		
		
	}

}

package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import ng_poc.Constants;


public class ExcelPack {
	
	public static XSSFWorkbook ExcelWrkbook;
	public static XSSFSheet Excelsheet;
	public static XSSFRow ExcelRow;
	public static XSSFCell ExcelCell;
	public static HashMap<String, String> excel_table = new HashMap<>();
	public static String ExcelPath;
	public static String SheetName;
	public static int testcasename_counter = 0;
	
	public static void setExcelFile(String ExcelPath, String SheetName) throws InvocationTargetException {
		
		try {
			FileInputStream Excelfile = new FileInputStream(ExcelPath);
			ExcelWrkbook = new XSSFWorkbook(Excelfile);
			Excelsheet = ExcelWrkbook.getSheet(SheetName);
			 
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static String getCelldata(int row_no, int col_no) {
		
		try {
			ExcelPack.setExcelFile(Constants.Testdata_path + Constants.Testdata_file, "TestData");
			ExcelCell = Excelsheet.getRow(row_no).getCell(col_no);
			String Celldata = ExcelCell.getStringCellValue();
			return Celldata;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public static void setCellData(String Celldata,int row_no, int col_no) throws IOException {
		
		ExcelRow = Excelsheet.getRow(row_no);
		ExcelCell = ExcelRow.getCell(col_no);
		if (ExcelCell == null) {
			ExcelCell = ExcelRow.createCell(col_no);
			ExcelCell.setCellValue((String)Celldata);
			
		}
		
		try {
			FileOutputStream fileout = new FileOutputStream(Constants.Testdata_path + Constants.Testdata_file);
			ExcelWrkbook.write(fileout);
			fileout.flush();
			fileout.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static void extractData() {
		
		int i = 0;
		try {
			ExcelPack.setExcelFile(Constants.Testdata_path + Constants.Testdata_file, "TestCase Manager");
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int last_row = Excelsheet.getLastRowNum();
		int last_col = Excelsheet.getRow(i).getLastCellNum();
		
		for (i = 0; i < last_row; i++) {
			for (int j = 0; j < last_col; j++) {
				excel_table.put(getCelldata(0, j),getCelldata(i, j));
				System.out.println(getCelldata(i, j));
			}
			
		}
	}
	
	public static String gettestcasename() {
		try {
			ExcelPack.setExcelFile(Constants.Testdata_path + Constants.Testdata_file, "TestCase Manager");
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String captured_value = ExcelPack.getCelldata(1, testcasename_counter);
		testcasename_counter++;
		return captured_value;
		
	}
	

	

}

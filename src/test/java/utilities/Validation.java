package utilities;

import static org.testng.Assert.assertEquals;

import com.relevantcodes.extentreports.LogStatus;

import ng_poc.Setup;

public class Validation extends Setup {
	
	static String current_title;
	
	public static void check_title(String expected_title) {
		
		try {
			current_title = driver.getTitle();
			assertEquals(current_title, expected_title);
			test_report.log(LogStatus.PASS, "URL" + expected_title + " Launched successfully");
		} catch (AssertionError e) {
			test_report.log(LogStatus.FAIL, "URL not launched");
		}
		
	}

}

package utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ng_poc.Setup;

public class Custom_Actions extends Setup {

	//wait for a particular element
	public static void axn_waitForElement(int seconds, String waitConditionLocator){
	    WebDriverWait wait = new WebDriverWait(driver, seconds*1000);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(waitConditionLocator)));
	}
	
	public static void axn_clickonelement_xpath(String elem_xpath) {
		
		axn_waitForElement(30,prop.getProperty(elem_xpath));
		driver.findElement(By.xpath(prop.getProperty(elem_xpath))).click();
	}
	
	public static void axn_entertextbox_xpath(String elem_xpath,String datatoenter) {
		
		driver.findElement(By.xpath(prop.getProperty(elem_xpath))).sendKeys(datatoenter);
	}
	
	public static void waitforelementonscreen(int seconds,String element_xpath,String typeofwait) {
		
		waitobj = new WebDriverWait(driver, seconds);
		switch (typeofwait) {
		case "just_wait":
			waitobj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element_xpath)));
			break;
		case "wait_and_click":
			WebElement click_elementtowait = waitobj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element_xpath)));
			click_elementtowait.click();
			break;
			
		case "wait_and_set":
			WebElement set_elementtowait = waitobj.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(element_xpath)));
			set_elementtowait.click();
			break;

		default:
			System.out.println("Invalid type of wait.. please revisit");
			break;
		}
		
		
	}
	
	
	
}
